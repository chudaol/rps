(function(ns) {
    /**
     * Returns an HTML string for the start game menu
     *
     * @param {object} templateObj
     * @returns {string}
     */
    ns.templates.startGameTmpl = function (templateObj) {
        var template;

        if (!templateObj) {
            templateObj = {};
        }

        template = "";
        template += "<div class=\"text-center\">";
        template +=     "<button class=\"yellow-background start user\">Play</button>";
        template +=     "<br/>";
        template +=     "<button class=\"yellow-background start computer\">Watch Computer Playing</button>";
        template +=     "</div>";

        return template;
    };
})(rps);