(function(ns) {
    /**
     * Template for the already played games
     *
     * @param {object} templateObj
     * @returns {string}
     */
    ns.templates.scoresTmpl = function (templateObj) {

        var template, games;

        template = "<button title=\"Close Menu\" class=\"yellow-background left close\"></button>";
        games = templateObj.models;
        if (games.length === 0) {
            template += '<div class="text-center">No available scores yet!</div>';
        } else {
            games.forEach(function (game) {
                template += '<h2>Mode: ' + game.mode + '</h2>';
                template += ns.templates.playedGameTmpl(game);
                template += '<hr/>';
            });
        }

        return template;
    };
})(rps);