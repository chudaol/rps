(function(ns) {
    /**
     * Returns an HTML string for the game over view
     *
     * @param {object} templateObj
     * @returns {string}
     */
    ns.templates.gameOverTmpl = function (templateObj) {
        var template = '';

        template += ns.templates.playedGameTmpl(templateObj);

        template += '<div><button class="yellow-background play-again">Play again!</button></div>';

        return template;
    };
})(rps);