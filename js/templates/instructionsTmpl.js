(function(ns) {
    /**
     * Returns an HTML string for the instructions menu
     *
     * @returns {string}
     */
    ns.templates.instructionsTmpl = function () {
        var template;

        template="<button title=\"Close Menu\" class=\"yellow-background right close\"></button>";
        template += "          <h1>Rules: (from <a href=\"http:\/\/en.wikipedia.org\/wiki\/Rock-paper-scissors\">wiki<\/a>)<\/h1>";
        template += "          <div class=\"text-center\">";
        template += "            <div class=\"item\"><img src=\"img\/wiki.png\"><\/div>";
        template += "          <\/div>";
        template += "          <ul>";
        template += "            <li>";
        template += "              Choose the game mode (user or computer)";
        template += "            <\/li>";
        template += "            <li>If the selected mode is user, please choose a hand to play with (stone, rock or scissor)<\/li>";
        template += "            <li>At the end of the game you can return to the initial screen<\/li>";
        template += "            <li>At each point you can refresh the scores area to see the previous games<\/li>";
        template += "          <\/ul>";

        return template;
    };
})(rps);