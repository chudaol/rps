(function(ns) {
    /**
     * Template for the already played game
     *
     * @param {object} templateObj
     * @returns {string}
     */
    ns.templates.playedGameTmpl = function (templateObj) {
        var template = '';

        template += '<h2>' + templateObj.won + ' won!' + '</h2>';
        template += '<div class="text-center">';
        template += '<div class="item">' + templateObj.user1 + '<img src="img/'+ templateObj.user1TurnItem.imgName + '">' + '</div>';
        template += '<div class="item">' + templateObj.user2 + '<img src="img/'+ templateObj.user2TurnItem.imgName + '">' + '</div>';

        template += '</div>';

        return template;
    };
})(rps);