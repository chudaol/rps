(function(ns) {
    /**
     * Returns an HTML string for the game view
     *
     * @param {object} templateObj
     * @returns {string}
     */
    ns.templates.playTmpl = function (templateObj) {
        var template = '';

        template += '<h2>' + templateObj.user1 + '</h2>';
        template += '<div class="text-center">';
        templateObj.items.models.forEach(function (item) {
            template += '<div class="item"><img alt="' + item.name +'" title="' + item.name + '" class="game-item" data-name="' + item.name + '" src="img/' + item.imgName + '"></div>';
        });
        template += "</div>";

        return template;
    };
})(rps);