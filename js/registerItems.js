var registeredItems = [
    {
        name: "scissors",
        imgName: "scissors.png",
        beats: "paper"
    },
    {
        name: "paper",
        imgName: "paper.png",
        beats: "stone"
    },
    {
        name: "stone",
        imgName: "stone.png",
        beats: "scissors"
    }
    // ADD YOUR NEW ITEMS HERE
];