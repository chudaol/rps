(function(ns) {
    /**
     * Renders the instructions on the right
     *
     * @constructor
     */
    ns.views.InstructionsView = function () {
        var div;

        div = document.createElement("div");
        div.className = "instructions-inner";
        document.getElementById("instructions").innerHTML = "";
        document.getElementById("instructions").appendChild(div);
        ns.views.View.call(this, div, null, null, ns.templates.instructionsTmpl);
        this.events = {
            "click close right": this.close
        };
    };
})(rps);