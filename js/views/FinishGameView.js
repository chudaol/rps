(function(ns) {
    /**
     * Renders a finished game
     *
     * @param {rps.models.Game} model the game model
     * @param {rps.collections.Items} play items like stone, scissors, paper.
     * @param {rps.collections.Games} games already played games
     *
     * @constructor
     */
    ns.views.FinishGameView = function(model, items, games) {
        var $content;

        $content = document.getElementById("content");
        ns.views.View.call(this, $content, ns.models.Game, model, ns.templates.gameOverTmpl);
        this.items = items;
        this.games = games;
        /**
         * Handles click on play again button
         * @private
         */
        this._onClickPlayAgain = function () {
            new ns.views.StartGameView(this.items, this.games).render();
        }.bind(this);
        /**
         * @overrides
         */
        this.events = {
            "click play-again": this._onClickPlayAgain
        };
    }
})(rps);