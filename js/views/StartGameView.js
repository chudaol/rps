(function(ns) {
    /**
     * The view that appears in the start of the game
     *
     * @returns {View}
     * @constructor
     */
    ns.views.StartGameView = function (items, games) {
        var $content;

        $content = document.getElementById("content");
        ns.views.View.call(this, $content, null, null, ns.templates.startGameTmpl);
        this.items = items;
        this.games = games;
        /**
         * Renders the finished current game
         * @param {Game} model
         */
        this.finishGame = function (model) {
            var finishGameView = new ns.views.FinishGameView(model, this.items, this.games);
            this.games.add(model);
            finishGameView.render();
        }.bind(this);
        /**
         * Invokes Game view when start button is clicked
         * @param {string} mode the mode of playing (user/computer)
         */
        this.startGame = function (mode) {

            this.gameView = new ns.views.GameView(new ns.models.Game({
                mode: mode,
                items: this.items,
                user1: mode === ns.enums.MODE.USER ? ns.enums.USERS.USER.USER1 : ns.enums.USERS.COMPUTER.USER1,
                user2: mode === ns.enums.MODE.USER ? ns.enums.USERS.USER.USER2 : ns.enums.USERS.COMPUTER.USER2,
                onfinish: this.finishGame
            }));
            this.gameView.render();
            if (mode === ns.enums.MODE.COMPUTER) {
                this.gameView.playRandom();
                this.gameView.playRandom();
            }
        };
        /**
         * Handles the click on the start button
         * @param {Event} ev
         * @private
         */
        this._onClickStart = function (ev) {
            var mode;

            mode = ev.currentTarget.classList.contains("user") ? ns.enums.MODE.USER : ns.enums.MODE.COMPUTER;
            this.startGame(mode);
        };
        /**
         * @overrides
         */
        this.events = {
            "click start": this._onClickStart
        };
    }
})(rps);