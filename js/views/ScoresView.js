(function(ns) {
    /**
     * Renders the scores on the left
     *
     * @param {rps.collections.Games} games the played games which scores are to be represented
     * @constructor
     */
    ns.views.ScoresView = function (games) {
        var div;

        this.games = games;
        div = document.createElement("div");
        div.className = "scores-inner";
        document.getElementById("scores").innerHTML = "";
        document.getElementById("scores").appendChild(div);
        ns.views.View.call(this, div, ns.collections.Collection, this.games, ns.templates.scoresTmpl);
        this.events = {
            "click close left": this.close
        };
    };
})(rps);