(function(ns) {
    /**
     * The view which appears when the user clicks on the play button
     *
     * @param {rps.models.Game} gameModel
     * @returns {rps.views.View}
     * @constructor
     */
    ns.views.GameView = function (gameModel) {
        var helper, $content;

        $content = document.getElementById("content");
        ns.views.View.call(this, $content, ns.models.Game, gameModel, ns.templates.playTmpl);

        this.model = gameModel;
        this.items = gameModel.items;
        helper = new ns.Helper();
        /**
         * Plays random
         */
        this.playRandom = function () {
            var randomItem;

            randomItem = helper.selectRandomItemFromTheList(this.model.items.models);
            this.model.play(randomItem);
        };
        /**
         * Handles the clicking on the item
         * @param {Event} ev
         * @private
         */
        this._onClickItem = function (ev) {
            var itemName, item;

            itemName = ev.currentTarget.dataset.name;
            item = this.items.get(itemName);
            this.model.play(item);
            this.unbindEvents();
            this.playRandom();
        };
        /**
         * @overrides
         */
        this.events = {
            "click game-item": this._onClickItem
        };
    }
})(rps);