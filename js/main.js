(function (ns) {
    "use strict";
    var items, games, view, imgs, helper;
    imgs = [];
    helper = new ns.Helper();
    // Initialize items
    items = new ns.collections.Items(registeredItems);
    // Initialize games
    games = new ns.collections.Games([]);
    // Start Rendering
    view = new ns.views.StartGameView(items, games);
    view.render();
    // Register global events
    document.getElementsByClassName("scores-button")[0].onclick = function () {
        new ns.views.ScoresView(games).render();
    };
    document.getElementsByClassName("instructions-button")[0].onclick = function () {
        new ns.views.InstructionsView().render();
    };
    items.models.forEach(function (item) {
        imgs.push("img/" + item.imgName);
    });
    imgs.push("img/wiki.png");
    helper.preloadImages(imgs);
})(rps);