(function(ns) {
    /**
     * Items collection
     *
     * @param {array} models
     * @returns {rps.collections.Collection}
     * @constructor
     */
    ns.collections.Items = function (models) {
        return new ns.collections.Collection(models, ns.models.Item);
    }
})(rps);