(function(ns) {
    /**
     * Games collection
     *
     * @param {array} models
     * @returns {rps.collections.Collection}
     * @constructor
     */
    ns.collections.Games = function (models) {
        return new ns.collections.Collection(models, ns.models.Game);
    }
})(rps);