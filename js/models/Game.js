(function(ns) {
    /**
     * Game model
     *
     * @param {object} model
     * @constructor
     */
    ns.models.Game = function(model) {
        this.mode = model.mode ? model.mode : ns.enums.MODE.USER;
        this.user1 = model.user1 || (this.mode === ns.enums.MODE.USER ? ns.enums.USERS.USER.USER1 : ns.enums.USERS.COMPUTER.USER1);
        this.user2 = model.user2 || (this.mode === ns.enums.MODE.USER ? ns.enums.USERS.USER.USER2 : ns.enums.USERS.COMPUTER.USER2);
        this.items = model.items || registeredItems;
        this.won = model.won;
        /**
         * Play the game with chosen item
         * @param {Item} withItem
         */
        this.play = function (withItem) {
            if (this.user1TurnItem === undefined) {
                this.user1TurnItem = withItem;
            } else if (this.user2TurnItem === undefined) {
                this.user2TurnItem = withItem;
                this.finish();
            }
        };
        /**
         * Finishes the game and invokes onfinish function
         */
        this.finish = function () {
            if (this.user1TurnItem.beats === this.user2TurnItem.name) {
                this.won = this.user1;
            } else if (this.user2TurnItem.beats === this.user1TurnItem.name){
                this.won = this.user2;
            } else {
                this.won = ns.enums.USERS.JUSTICE;
            }
            model.onfinish(this);
        }
    }
})(rps);