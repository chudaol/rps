(function(ns) {
    /**
     * Item model
     *
     * The playing item like stone/scissors/paper
     *
     * @param {Object} model
     * @constructor
     */
    ns.models.Item = function (model) {
        this.beats = model.beats;
        this.name = model.name;
        this.imgName = model.imgName;
    }
})(rps);
