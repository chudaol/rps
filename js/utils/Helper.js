(function(ns) {
    /**
     * Helper functions
     * @returns {object} an object of helper functions
     */
    ns.Helper = function () {
        return {
            /**
             *  Assigns events handlers to the class name
             *
             * @param {Dom element} element father of the elements to assign handlers
             * @param {string} eventName the event name to assign (onclick/onhover/etc)
             * @param {string} className the class name of the elements to assign this handler
             * @param {function} handler handler function
             * @param {object} [scope] scope where the function resides
             */
            assignEventsHandlersToClassName: function (el, eventName, className, handler, scope) {
                var elements;

                elements = el.getElementsByClassName(className);
                [].forEach.call(elements, function (element) {
                    var listener;

                    listener = scope ? handler.bind(scope) : handler;
                    element.addEventListener(eventName, listener);
                });
            },
            /**
             * Removes events handlers from the elements with the given class name
             *
             * @param {Dom element} element father of the elements to remove handlers
             * @param {string} eventName the event name which handlers should be removed
             * @param {string} className the class name of the elements from which the handler should be removed
             * @param {function} handler the handler function to remove from the given elements
             * @param {object} [scope] the scope owner of the callback function
             */
            removeEventsHandlersFromClassName: function (el, eventName, className, handler, scope) {
                var elements;

                elements = el.getElementsByClassName(className);
                [].forEach.call(elements, function (element) {
                    var listener;

                    listener = scope ? handler.bind(scope) : handler;
                    element.removeEventListener(eventName, listener);
                });
            },
            /**
             * Returns a randomly chosen item from the list of items
             *
             * @param {array} list
             * @returns {*}
             */
            selectRandomItemFromTheList: function (list) {
                var length, randomIndex;

                length = list.length;
                randomIndex = Math.floor(Math.random() * length);
                return list[randomIndex];
            },
            /**
             * Preloads images
             * @param {array} images images names
             */
            preloadImages: function (images) {
                if (document.images) {
                    images.forEach(function(image) {
                        var imageObj = new Image();
                        imageObj.src = image;
                    });
                }
            }
        }
    }
})(rps);