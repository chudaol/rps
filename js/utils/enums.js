(function(ns) {
    /**
     * Enumeration for different game modes
     *
     * @type {{USER: string, COMPUTER: string}}
     */
    ns.enums.MODE = {
        USER: "user",
        COMPUTER: "computer"
    };
    /**
     * Enumeration for different user's names
     *
     * @type {{USER: {USER1: string, USER2: string}, COMPUTER: {USER1: string, USER2: string}, JUSTICE: string}}
     */
    ns.enums.USERS = {
        USER: {
            USER1: "You",
            USER2: "Computer"
        },
        COMPUTER: {
            USER1: "Computer1",
            USER2: "Computer2"
        },
        JUSTICE: "Justice"
    };
})(rps);