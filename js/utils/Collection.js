(function(ns) {
    /**
     * Generic Collections Constructor
     *
     * @param {array} models array of the models to initialize the collection
     * @param {*} Model model's constructor
     * @constructor
     */
    ns.collections.Collection = function (models, Model) {
        this.Model = Model;
        this.models = [];
        models.forEach(function (model) {
            if (model instanceof Model) {
                this.models.push(model);
            } else {
                this.models.push(new Model(model));
            }
        }.bind(this));
        /**
         * Adds a new model to the collection
         *
         * @param model
         */
        this.add = function (model) {
            this.models.push(model instanceof this.Model ? model : new this.Model(model));
        };
        /**
         * Returns a model by its name
         *
         * @param {string} name
         * @returns {*}
         */
        this.get = function (name) {
            var findModel;

            this.models.some(function (model) {
                findModel = model;
                return model.name === name;
            });
            return findModel;
        }
    }
})(rps);