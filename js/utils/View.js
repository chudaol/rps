(function(ns) {
    /**
     * Generic View Constructor
     *
     * @param {Element} el element on which the view should be rendered
     * @param {*} Model model constructor
     * @param {*} [model] the model object
     * @param {function} template template function
     * @constructor
     */
    ns.views.View = function (el, Model, model, template) {
        var helper;

        this.events = {};
        this.$el = el;
        this.template = template;
        helper = new ns.Helper();
        /**
         * Initializes the view with the given model
         * @param {*} model
         */
        this.initialize = function (model) {
            if (!model) {
                model = {};
            }
            this.model = !Model || model instanceof Model ? model : new Model(model);
        };
        this.initialize(model);
        /**
         * Renders the view
         */
        this.render = function () {
            this.$el.innerHTML = this.template(this.model);
            this.bindEvents();
        };
        /**
         * Binds events
         */
        this.bindEvents = function () {
            for (var eventString in this.events) {
                var eventClassNameArray, eventName, className, handler;

                if (this.events.hasOwnProperty(eventString)) {
                    handler = this.events[eventString];
                    eventClassNameArray = eventString.split(" ");
                    eventName = eventClassNameArray[0];
                    className = eventClassNameArray[1];
                    helper.assignEventsHandlersToClassName(this.$el, eventName, className, handler, this);
                }
            }
        };
        /**
         * Unbinds events
         */
        this.unbindEvents = function () {
            for (var eventString in this.events) {
                var eventClassNameArray, eventName, className, handler;

                if (this.events.hasOwnProperty(eventString)) {
                    handler = this.events[eventString];
                    eventClassNameArray = eventString.split(" ");
                    eventName = eventClassNameArray[0];
                    className = eventClassNameArray[1];
                    helper.removeEventsHandlersFromClassName(this.$el, eventName, className, handler);
                }
            }
        };
        /**
         * Closes the view
         */
        this.close = function () {
            this.unbindEvents();
            this.$el.parentElement.removeChild(this.$el);
        }
    }
})(rps);