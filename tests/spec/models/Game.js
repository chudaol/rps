/*global describe, spyOn, it, beforeEach, afterEach, expect*/
describe('Game', function () {
    describe('initialize', function () {
        it('should correctly initialize a game model with passed options', function () {
            var game;

            game = new rps.models.Game({
                mode: "other",
                user1: "Bob",
                user2: "Alice",
                items: [{name: "item"}],
                won: false
            });

            expect(game.mode).toEqual("other");
            expect(game.items).toEqual([{name: "item"}]);
        });

        it('should correctly initialize a game model with default values', function () {
            var game;

            game = new rps.models.Game({});
            expect(game.mode).toEqual(rps.enums.MODE.USER);
            expect(game.user1).toEqual(rps.enums.USERS.USER.USER1);
            expect(game.user2).toEqual(rps.enums.USERS.USER.USER2);
        });
    });

    describe('play', function () {
        it('should set user1TurnItem and not call finish function', function () {
            var game;

            game = new rps.models.Game({});
            spyOn(game, "finish");
            game.play("turn");
            expect(game.user1TurnItem).toEqual("turn");
            expect(game.finish).not.toHaveBeenCalled();
        });

        it('should set user2TurnItem and call finish function', function () {
            var game;

            game = new rps.models.Game({});
            spyOn(game, "finish");
            game.user1TurnItem = "turn";
            game.play("turn2");
            expect(game.user2TurnItem).toEqual("turn2");
            expect(game.finish).toHaveBeenCalled();
        });
    });

    describe('finish', function () {
        it('should set won on the first user', function () {
            var game;

            game = new rps.models.Game({user1: "Bob", onfinish: function () {}});
            game.user1TurnItem = {name: "a", beats: "b"};
            game.user2TurnItem = {name: "b"};
            game.finish();
            expect(game.won).toEqual("Bob");
        });

        it('should set won on the second user', function () {
            var game;

            game = new rps.models.Game({user1: "Bob", user2: "Alice", onfinish: function () {}});
            game.user1TurnItem = {name: "a", beats: "c"};
            game.user2TurnItem = {name: "b", beats: "a"};
            game.finish();
            expect(game.won).toEqual("Alice");
        });

        it('should set won on justice and call onfinish function', function () {
            var game, model;

            model = {user1: "Bob", user2: "Alice", onfinish: function () {}};
            game = new rps.models.Game(model);
            spyOn(model, "onfinish");
            game.user1TurnItem = {name: "a", beats: "c"};
            game.user2TurnItem = {name: "a", beats: "c"};
            game.finish();
            expect(game.won).toEqual(rps.enums.USERS.JUSTICE);
            expect(model.onfinish).toHaveBeenCalled();
        });
    });


});