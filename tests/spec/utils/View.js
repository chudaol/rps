/*global describe, it, beforeEach, afterEach, expect*/
describe('View', function () {
    var div, template;
    function Person(model) {
        this.name = model.name;
    }
    template = function (model) {
        return "<h1>Hello " + model.name + "</h1>";
    };
    beforeEach(function () {
        div = document.createElement("div");
        document.body.appendChild(div);
        div.innerHTML = "<div id=\"instructions\"></div>";
    });
    afterEach(function () {
        document.body.removeChild(div);
    });
    describe('initialize', function () {
        it('should create a new model of the given class', function () {
            var view;

            view = new rps.views.View(div, Person, undefined, template);
            expect(view.model instanceof Person).toBe(true);
        });
    });
    describe('render', function () {
        it('should render a given template for a given model', function () {
            var view;

            view = new rps.views.View(div, Person, {name: "Bob"}, template);
            view.render();
            expect(div.innerHTML).toEqual("<h1>Hello Bob</h1>")
        });
    });
});