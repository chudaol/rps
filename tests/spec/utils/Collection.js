/*global describe, it, beforeEach, afterEach, expect*/
describe('Collection', function () {
    var people;

    function Person(model) {
        this.name = model.name;
    }
    people = [{name: "Bob"}, {name: "Alice"}, {name: "Olga"}];

    describe('initialize', function () {

        it('should correctly initialize a collection with an array of passed models', function () {
            var collection;

            collection = new rps.collections.Collection(people, Person);
            expect(collection.models[0]).toEqual(new Person({name: "Bob"}));
        });
    });

    describe('add', function () {
        it('should correctly add a new model of instance of Person', function () {
            var collection, length;

            collection = new rps.collections.Collection(people, Person);
            length = collection.models.length;
            collection.add({name: "Alice"});
            expect(collection.models.length).toEqual(length + 1);
            expect(collection.get("Alice") instanceof Person).toBe(true);
        });
    });

    describe('get', function () {
        it('should correctly get the models by their names', function () {
            var collection;

            collection = new rps.collections.Collection(people, Person);
            expect(collection.get("Bob")).toEqual(new Person({name: "Bob"}));
        });
    });
});