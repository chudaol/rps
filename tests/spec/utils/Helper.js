/*global describe, it, beforeEach, afterEach, expect*/
describe('InstructionsView', function () {
    var div, helper, scope;

    beforeEach(function () {
        div = document.createElement("div");
        document.body.appendChild(div);
        div.innerHTML = "<div class=\"myClass\"></div><div class=\"myClass\"></div>"
        helper = new rps.Helper();
        scope = {
            callback: function () {
            }
        }
    });
    afterEach(function () {
        document.body.removeChild(div);
    });
    describe('assignEventsHandlersToClassName', function () {
        it('should assign the event handler to the given class', function () {
            spyOn(scope, "callback");
            helper.assignEventsHandlersToClassName(div, "click", "myClass", scope.callback, scope);
            div.getElementsByClassName("myClass")[0].click();
            expect(scope.callback).toHaveBeenCalled();
        });
    });

    describe('removeEventsHandlersFromClassName', function () {
        it('should not call the callback function after this has being removed', function () {
            spyOn(scope, "callback");
            helper.assignEventsHandlersToClassName(div, "click", "myClass", scope.callback);
            helper.removeEventsHandlersFromClassName(div, "click", "myClass", scope.callback);
            div.getElementsByClassName("myClass")[0].click();
            expect(scope.callback).not.toHaveBeenCalled();
        });
    });
});