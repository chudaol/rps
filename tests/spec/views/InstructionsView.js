/*global describe, it, beforeEach, afterEach, expect*/
describe('InstructionsView', function () {
    var div;

    beforeEach(function () {
        div = document.createElement("div");
        document.body.appendChild(div);
        div.innerHTML = "<div id=\"instructions\"></div>";
    });
    afterEach(function () {
        document.body.removeChild(div);
    });
    describe('close', function () {
        it('should close the view when the close method is called', function () {
            var view;

            view = new rps.views.InstructionsView();
            view.render();
            expect(document.getElementsByClassName("instructions-inner").length).toEqual(1);
            view.close();
            expect(document.getElementsByClassName("instructions-inner").length).toEqual(0);
        });
    });
});