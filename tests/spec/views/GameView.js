/*global describe, spyOn, it, beforeEach, afterEach, expect*/
describe('GameView', function () {
    var div;

    beforeEach(function () {
        div = document.createElement("div");
        document.body.appendChild(div);
        div.innerHTML = "<div id = \"content\"></div>";
    });

    afterEach(function () {
        document.body.removeChild(div);
    });
    describe('initialize', function () {
        it('should correctly initialize the view', function () {
            var view;

            view = new rps.views.GameView(new rps.models.Game({}));
            expect(view.model.mode).toEqual(rps.enums.MODE.USER);
        });
    });

    describe('playRandom', function () {
        it('should call play on the gameModel with the only item in the list', function () {
            var view, gameModel;

            gameModel = new rps.models.Game({items: {models: [1]}});
            gameModel.play = function (item) {};
            spyOn(gameModel, "play");
            view = new rps.views.GameView(gameModel);
            view.playRandom();
            expect(gameModel.play).toHaveBeenCalledWith(1);
        });
    });

    describe('_onClickItem', function () {
        it('should call play on the model with the clicked item', function () {
            var view, gameModel;

            gameModel = new rps.models.Game({items: new rps.collections.Items([{beats: "a", imgName: "a", name: "a"}, {name: "b"}])});
            gameModel.play = function (item) {};
            spyOn(gameModel, "play");
            view = new rps.views.GameView(gameModel);
            view._onClickItem({currentTarget: {dataset: {name: "a"}}});
            expect(gameModel.play).toHaveBeenCalledWith(new rps.models.Item({beats: 'a', name: 'a', imgName: 'a'}));
        });
    });
});