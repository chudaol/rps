# Rock, Paper, Scissors

The project developed by Olga Filipova as the code puzzle for Ebay


## Quick start

Please open the file index.html in your favourite browser. Read the instructions and play!

* Target browser: Chrome
* Supported browsers: all majors browsers
* Tested browsers: Chrome, Firefox, Safari, mobile Safari


## Adding new items

If you want to extend the game and add more items do the following:

* Add the image for your new item to the img/ folder
* Open the file registerItems.js
* Add new item in the same format as the other ones.
** Specify it's name, the added image's name and whom does it beat.
** Also do not forget to change some of the existing items so somebody also beats your newly added item!
* Enjoy!

## Tests

To run tests just open the html file tests/index.html in your favourite browser